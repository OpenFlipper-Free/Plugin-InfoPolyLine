/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



//=============================================================================
//
//  CLASS InfoMeshObjectPlugin - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================


#include "PolyLineObjectInfoPlugin.hh"

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <MeshTools/MeshInfoT.hh>
#include <ACG/Geometry/Algorithms.hh>


//== IMPLEMENTATION ==========================================================

void InfoPolyLineObjectPlugin::initializePlugin() {

  //if ( OpenFlipper::Options::gui()) {
    // Create info dialog
    info_ = new InfoDialog();
  //}

}

/// initialize the plugin
void InfoPolyLineObjectPlugin::pluginsInitialized() {

}

//-----------------------------------------------------------------------------

DataType InfoPolyLineObjectPlugin::supportedDataTypes() {
    return DataType(DATA_POLY_LINE | DATA_POLY_LINE_COLLECTION);
}

//-----------------------------------------------------------------------------

void InfoPolyLineObjectPlugin::printPolyLineInfo( PolyLine* _polyLine,  unsigned int _objectId, unsigned int _index, const ACG::Vec3d& _hitPoint ) {

    QLocale locale;
    QString name;


//    auto min_dist = 9e9f;
//    size_t idx = -1u;
//    /// find closest vertex to hitpoint
//    for(auto i = 0u;  i < _polyLine->n_vertices();++i)
//    {
//        const auto dist =  sqrt((_polyLine->point(i) - _hitPoint).sqrnorm());
//        if(dist < min_dist)
//        {
//            min_dist = dist;
//            idx = i;
//        }
//    }
//    /// we found something
//    if(idx != -1u) // maybe add threshold
//    {
//        std::cout << "Found vertex at: (" << _polyLine->point(idx)[0] << ", " << _polyLine->point(idx)[1] << ", " << _polyLine->point(idx)[2] << ")" << std::endl;
//    }

    // name
    BaseObject* obj = 0;
    if ( PluginFunctions::getObject(_objectId, obj) )
        info_->generalBox->setTitle( tr("General object information for %1").arg( obj->name() ) );

    // ID
    info_->id->setText( locale.toString(_objectId) );

    // Joints
    info_->vertices->setText( QString::number( _polyLine->n_vertices() ) );

    // Clicked:
    info_->clickedHandle->setText( locale.toString( _index  ) );

    PolyLine::Point bbMin( FLT_MAX,  FLT_MAX,  FLT_MAX);
    PolyLine::Point bbMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    PolyLine::Point cog(0.0,0.0,0.0);

    for ( auto pos : _polyLine->points() ) {
        cog += pos;

        bbMin.minimize(pos);
        bbMax.maximize(pos);
    }

    //Bounding Box Size
    PolyLine::Point diff = bbMax-bbMin;

    info_->bbMinX->setText( QString::number(bbMin[0],'f') );
    info_->bbMinY->setText( QString::number(bbMin[1],'f') );
    info_->bbMinZ->setText( QString::number(bbMin[2],'f') );

    info_->bbMaxX->setText( QString::number(bbMax[0],'f') );
    info_->bbMaxY->setText( QString::number(bbMax[1],'f') );
    info_->bbMaxZ->setText( QString::number(bbMax[2],'f') );

    info_->bbSizeX->setText( QString::number(diff[0],'f') );
    info_->bbSizeY->setText( QString::number(diff[1],'f') );
    info_->bbSizeZ->setText( QString::number(diff[2],'f') );


    //COG
    cog = cog / _polyLine->n_vertices() ;

    info_->cogX->setText( QString::number(cog[0],'f') );
    info_->cogY->setText( QString::number(cog[1],'f') );
    info_->cogZ->setText( QString::number(cog[2],'f') );

    //hitpoint
    info_->pointX->setText( QString::number( _hitPoint[0],'f' ) );
    info_->pointY->setText( QString::number( _hitPoint[1],'f' ) );
    info_->pointZ->setText( QString::number( _hitPoint[2],'f' ) );

    info_->setWindowFlags(info_->windowFlags() | Qt::WindowStaysOnTopHint);

    info_->show();
}

void InfoPolyLineObjectPlugin::printPolyLineCollectionInfo(PolyLineCollection *_polyLineCollection, unsigned int _objectId, unsigned int _index, const ACG::Vec3d &_hitPoint)
{
    
    auto min_dist = 9e9f;
    size_t idx = -1u;
    size_t line = -1u;
    
    /// find closest vertex to hitpoint
    for(auto l = 0u;  l < _polyLineCollection->n_visible_polylines();++l)
    {
        const auto _polyLine = _polyLineCollection->polyline(l);
        for(auto i = 0u;  i < _polyLine->n_vertices();++i)
        {
            
            const auto dist =  sqrt((_polyLine->point(i) - _hitPoint).sqrnorm());
            if(dist < min_dist)
            {
                min_dist = dist;
                idx = i;
                line = l;
            }
        }
    }
    /// we found something
    if(idx != -1u) // maybe add threshold
    {
        auto const & plp = _polyLineCollection->polyline(line)->point(idx); /// get polyline point
        std::cout << "Found vertex at: (" << plp[0] << ", " << plp[1] << ", " << plp[2] << ")" << std::endl;
    }
}

//----------------------------------------------------------------------------------------------

void InfoPolyLineObjectPlugin::slotInformationRequested(const QPoint _clickedPoint, DataType _type) {

    // Only respond on skeleton objects
    if( _type != DATA_POLY_LINE && _type != DATA_POLY_LINE_COLLECTION ) return;

    ACG::SceneGraph::PickTarget target = ACG::SceneGraph::PICK_ANYTHING;

    size_t         node_idx, target_idx;
    ACG::Vec3d     hit_point;

    if (PluginFunctions::scenegraphPick(target, _clickedPoint, node_idx, target_idx, &hit_point)) {
      BaseObjectData* object;

      if ( PluginFunctions::getPickedObject(node_idx, object) ) {

         emit log( LOGINFO , object->getObjectinfo() );

         if ( object->picked(node_idx) && object->dataType(DATA_POLY_LINE) )
            printPolyLineInfo( PluginFunctions::polyLine(object) , object->id(), target_idx, hit_point );

         if ( object->picked(node_idx) && object->dataType(DATA_POLY_LINE_COLLECTION) )
             printPolyLineCollectionInfo(PluginFunctions::polyLineCollection(object) , object->id(), target_idx, hit_point );

      } else return;
    }
}


